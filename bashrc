source /Library/Developer/CommandLineTools/usr/share/git-core/git-prompt.sh
##### PS1 Prompt #####
# Colors
source $HOME/.bash_colors
# Various variables you might want for your PS1 prompt instead
Time12h="\T"
Time12a="\@"
PathFull="\w"
PathShort="\W"
NewLine="\n"
Jobs="\j"
Host="\h"
export PROMPT_DIRTRIM=1

function pyversion()
{
    local v=$(pyenv version | awk '{print $1}')
    if [ "$v" != "3.6.4" ]; then
      echo $v;
    else
      echo ""
    fi
}

export PS1='$(pyversion)'$Color_Off'$(git branch &>/dev/null;\
if [ $? -eq 0 ]; then \
  echo "$(echo `git status` | grep "nothing to commit" > /dev/null 2>&1; \
  if [ "$?" -eq "0" ]; then \
    # @4 - Clean repository - nothing to commit
    echo "'$Green'"$(__git_ps1 "(%s)"); \
  else \
    # @5 - Changes to working tree
    echo "'$IRed'"$(__git_ps1 "{%s}"); \
  fi) '$BYellow$PathFull$Color_Off'\$ "; \
else \
  # @2 - Prompt when not in GIT repo
  echo "'$Yellow$PathFull$Color_Off'\$ "; \
fi)'

##### ALIASES #####
alias gs='git status -s'
alias ls='gls -hFGl --color=auto'
alias df='dfc'
alias grep='grep --color'
alias curl='curl -s'
alias py2='pyenv shell 2.7.12'
alias py35='pyenv shell 3.5.5'
alias py36='pyenv shell 3.6.4'
alias pym='pyenv shell miniconda3-latest'
alias killdups='/System/Library/Frameworks/CoreServices.framework/Frameworks/LaunchServices.framework/Support/lsregister -kill -r -domain local -domain system -domain user;killall Finder;echo "Rebuilt Open With, relaunching Finder"'
title() {
    echo -n -e "\033]0;$1\007"

}

##### PATH #####
eval "$(pyenv init -)"
export PATH="$PATH:/usr/local/sbin"
export GOPATH=$HOME/Code/golang
#export GOROOT=/usr/local/opt/go/libexec/bin
export PATH=$PATH:$GOPATH/bin
#export PATH=$PATH:$GOROOT/bin

if [ -f $(brew --prefix)/etc/bash_completion ]; then
    . $(brew --prefix)/etc/bash_completion
fi
